import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import ElementUI from 'element-ui'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'element-ui/lib/theme-chalk/index.css'

import 'vue-search-select/dist/VueSearchSelect.css'
import { ModelListSelect } from 'vue-search-select'

import VueStar from 'vue-star-rating'

import VueSlider from 'vue-slider-component'
import 'vue-slider-component/theme/antd.css'



Vue.config.productionTip = false
Vue.component('SearchSelect', ModelListSelect)
Vue.component('VueSlider', VueSlider)
Vue.component('VueStar', VueStar)
Vue.use(ElementUI, {locale: 'ru'})


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
